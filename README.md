# Empresas App

## 🎯 Objetivo
O objetivo deste projeto é criar um aplicativo iOS, proposto como desafio técnico integrante do processo seletivo relacionado a vaga de Pessoa Desenvolvedora iOS na empresa ioasys.
O aplicativo desenvolvido cumpre os deafios apresentados na descrição presente ao final desse documento e seu layout segue o modelo do [Figma](https://bit.ly/testeempresasios) proposto.

## 🦅 O que foi explorado?

### Arquitetura
* O modelo base de arquitetura adotado foi o MVVM. Assim, cada módulo referente as telas apresentadas segue o seguinte padrão de divisão:
    * Services
    * ViewControllers
    * ViewModels
    * Views
* Para tornar os arquivos do projeto o mais independente entre si possível, foi utilizado o SPM em conjunto com Coordinator Pattern

### Layout
* O layout do projeto foi todo criado utilizando View Code, apenas a LaunchScreen do projeto foi feita com Storyboard. Essa decisão foi tomada para evitar problemas de conflito via git relacionados a arquivos de Storyboard, caso uma equipe trabalhe neste projeto no futuro.
* Todos os elementos foram construídos utilizando UIKit.

### Bibliotecas
* [Kingfisher](https://github.com/onevcat/Kingfisher) -> Utilizada para armazenamento de imagens vindas da internet em cache.

### Lógica de busca
* A lógica de busca das empresas listadas se baseia numa busca por nomes de empresa que coincidem com o texto digitado no campo de busca.

# Preview

<div align="center">
  <img src="https://user-images.githubusercontent.com/86199915/165027411-6ce78d21-b3b7-4c69-9d71-d48fd581ed87.gif" alt="App Launch Screen" style="width:15em;"/>
</div>

<br/>

![N|Solid](logo_ioasys.png)

# Desafio Pessoa Desenvolvedora iOS

## 🏗  O que fazer?
Você deve fazer um fork deste repositório, criar o código e ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, NÃO é necessário criar um Pull Request para isso, nós iremos avaliar e retornar por e-mail o resultado do seu teste. 

## 📱Escopo de projeto
Deve ser criado um aplicativo iOS utilizando Swift com as seguintes especificações:

* Login e acesso de Usuário já registrado
    * Para o login usamos padrões OAuth 2.0. Na resposta de sucesso do login a api retornará 3 custom headers (access-token, client, uid);
    * Para ter acesso as demais APIS precisamos enviar esses 3 custom headers para a API autorizar a requisição;
* Busca de Empresas
* Listagem de Empresas
* Detalhamento de Empresas

**Sinta-se a vontade para:**

* Escolher a arquitetura
* Usar ou não usar bibliotecas
* Estruturar seu layout com storyboards, xibs, view code (UIKit ou SwiftUI).

## 🕵 Itens a serem avaliados
Pense no desafio como uma oportunidade de mostrar todo o seu conhecimento. Independente de onde conseguiu chegar no teste, é importante disponibilizar sua implementação para analisarmos.

* Estrutura do projeto
* Consumo de APIs
* Lógicas de busca
* Estruturação de layout e fluxo de aplicação
* Utilização de código limpo e princípios SOLID
* Boas práticas da linguagem

## 🎁 Extra
Estes itens não são obrigatórios, porém desejados.

* Testes unitários
* Testes de UI
* Modularização

## 🚨 Informações Importantes
* Layout e recortes disponíveis no Figma (https://bit.ly/testeempresasios)
* Você deve fazer um cadastro no Figma para ter acesso ao layout.
* Integração disponível a partir de uma collection para Postman (https://www.getpostman.com/apps) disponível neste repositório.

## Dados para Teste
* Servidor: https://empresas.ioasys.com.br/api
* Versão da API: v1
* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234