import Foundation
import Core
import Networking
import Companies

struct CompaniesService: CompaniesServiceProtocol {
    func getCompanies(authToken: String, client: String, uid: String, companyName: String, completion: @escaping (Result<Companies, Error>) -> Void) {
        let request = CompaniesRequest(authToken: authToken, client: client, uid: uid, companyName: companyName)
        
        NetworkManager.shared.request(of: Companies.self, request: request) { result in
            switch result {
            case .success(let result):
                completion(.success(result))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
