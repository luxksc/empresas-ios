import Foundation
import Networking

struct CompaniesRequest: URLRequestProtocol {
    
    private var authToken: String
    private var client: String
    private var uid: String
    private var companyName: String
    
    init(authToken: String, client: String, uid: String, companyName: String) {
        self.authToken = authToken
        self.client = client
        self.uid = uid
        self.companyName = companyName
    }
    
    var baseRequestURL: String {
        return "\(Constants.apiBaseURL)enterprises"
    }
    
    var path: String? {
        return "name=\(companyName)"
    }
    
    var headers: [String: String]? {
        return [
            "Content-Type":"application/json",
            "access-token":authToken,
            "client":client,
            "uid":uid
        ]
    }
    
    var body : [String: String]? {
        return nil
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    
}
