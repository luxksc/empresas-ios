import Foundation
import UIKit
import Companies
import Core

final class CompaniesCoordinator: Coordinator {
    
    private(set) var childCoordinators: [Coordinator] = []
    
    private let authResponse: AuthResponse
    
    private let navigationController: UINavigationController
    
    private var companiesViewController: UINavigationController = UINavigationController()
    
    init(navigationController: UINavigationController, authResponse: AuthResponse) {
        self.navigationController = navigationController
        self.authResponse = authResponse
    }
    
    func start() {
        
        let companiesViewModel = CompaniesViewModel(authResponse: authResponse, getCompaniesService: CompaniesService(), pushToDetails: {company in self.pushToDetails(with: company)})
        
        companiesViewController = UINavigationController(rootViewController: CompaniesViewController(viewModel: companiesViewModel))
        companiesViewController.view.backgroundColor = UIColor.white
        companiesViewController.modalPresentationStyle = .fullScreen
        
        navigationController.present(companiesViewController, animated: true)
    }
    
    func pushToDetails(with company: Company) {
        
        let detailsCoordinator = DetailsCoordinator(navigationController: companiesViewController, company: company)
        
        childCoordinators.append(detailsCoordinator)
        
        detailsCoordinator.start()
        
    }
}
