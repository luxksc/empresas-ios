import UIKit

public class LoadingIndicator: UIActivityIndicatorView {

    public override init(frame: CGRect) {
        super.init(frame: frame)
        color = .black
        style = .large
        isHidden = true
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
