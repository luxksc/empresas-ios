import UIKit
import DesignSystem

public class InputTextField: UITextField {
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        //style
        textColor = UIColor.primary
        layer.cornerRadius = Metrics.Style.smallRadius
        layer.borderWidth = Metrics.Style.borderWidth
        addTarget(self, action: #selector(setEditingBorder), for: .editingDidBegin )
        addTarget(self, action: #selector(setDefaultBorder), for: .editingDidEnd)
        layer.borderColor = UIColor.textFieldNeutralBorder
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .white
        autocapitalizationType = .none
        leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: frame.height)) // add left padding to textfield
        leftViewMode = .always
        
        //constraints
        NSLayoutConstraint.activate([
            heightAnchor.constraint(equalToConstant: Metrics.Size.defaultHeight)
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func setEditingBorder() {
        layer.borderColor = UIColor.secondary.cgColor
    }
    
    @objc
    private func setDefaultBorder() {
        layer.borderColor = UIColor.textFieldNeutralBorder
    }
}
