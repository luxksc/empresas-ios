import UIKit
import DesignSystem

public class ErrorLabel: UILabel {

    public override init(frame: CGRect) {
        super.init(frame: frame)
        textColor = UIColor.error
        font = UIFont.systemFont(ofSize: Metrics.Size.buttonFont)
        isHidden = true
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
