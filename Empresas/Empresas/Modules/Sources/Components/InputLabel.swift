import UIKit
import DesignSystem

public class InputLabel: UILabel {

    public override init(frame: CGRect) {
        super.init(frame: frame)
        textColor = #colorLiteral(red: 0.3294117647, green: 0.3411764706, blue: 0.3490196078, alpha: 1)
        font = UIFont.boldSystemFont(ofSize: Metrics.Size.defaultFont)
        isHidden = true
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
