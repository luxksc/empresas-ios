import Foundation
import Core

public final class NetworkManager {
    
    public static let shared = NetworkManager()
    
    ///Makes an API call based on an request and the type of result expected
    public func request<T: Decodable>(of type: T.Type = T.self, request: URLRequestProtocol, completion: @escaping (Result<T, Error>) -> Void) {
        
        if var baseRequestURL = URLComponents(string: request.baseRequestURL) {
            
            //add parameters to request URL
            if let path = request.path {
                baseRequestURL.query = path
            }
            
            let requestURL = createRequestURL(from: baseRequestURL, request: request)
            
            let dataTask = URLSession.shared.dataTask(with: requestURL) { data, response, error in
                
                if let error = error {
                    completion(.failure(error))
                }
                
                guard let httpResponse = response as? HTTPURLResponse else { return }
                
                switch httpResponse.statusCode {
                case 200:
                    do {
                        switch request.method {
                        case .post:
                            let authResponse = self.getAuthResponse(httpResponse: httpResponse) as! T
                            
                            completion(.success(authResponse))
                        case .put:
                            break
                        case .get:
                            guard let data = data else { return }
                            
                            let decoder = JSONDecoder()
                            let responseData = try decoder.decode(type, from: data)
                            
                            completion(.success(responseData))
                        case .delete:
                            break
                        case .patch:
                            break
                        }
                    } catch {
                        completion(.failure(error))
                    }
                case 401:
                    completion(.failure(HTTPError.unauthorized))
                case 304:
                    completion(.failure(HTTPError.notModified))
                case 422:
                    completion(.failure(HTTPError.unprocessableEntity))
                case 503:
                    completion(.failure(HTTPError.serviceUnavailable))
                default:
                    break
                }
            }
            
            dataTask.resume()
        }
    }
    
    //MARK: - Private methods
    
    ///Create request url based on its headers and body
    private func createRequestURL(from baseRequestURL: URLComponents, request: URLRequestProtocol) -> URLRequest {
        
        guard let url = baseRequestURL.url else { return URLRequest(url: URL(fileURLWithPath: "NoURL"))}
        
        var requestURL = URLRequest(url: url)
        
        requestURL.httpMethod = request.method.name
        
        //add body to request url
        if let body = request.body {
            let jsonBody = try! JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            requestURL.httpBody = jsonBody
        }
        
        //add headers to request url
        if let headers = request.headers {
            requestURL.allHTTPHeaderFields = headers
        }
        
        return requestURL
    }
    
    ///Extract necessary keys for calling companies api from httpresponse headers
    private func getAuthResponse(httpResponse: HTTPURLResponse) -> AuthResponse {
        let accessToken = httpResponse.value(forHTTPHeaderField: "access-token")
        let client = httpResponse.value(forHTTPHeaderField: "client")
        let uid = httpResponse.value(forHTTPHeaderField: "uid")
        
        let authResponse = AuthResponse(accessToken: accessToken ?? "NoToken", client: client ?? "NoClient", uid: uid ?? "NoUID")
        
        return authResponse
    }
    
}
