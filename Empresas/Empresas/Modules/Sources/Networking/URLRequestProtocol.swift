import Foundation

public protocol URLRequestProtocol {
    
    /// The API's base url request
    var baseRequestURL: String { get }
    
    /// Defines the endpoint we want to hit
    var path: String? { get }
    
    /// Defines the request headers
    var headers: [String: String]? { get }
    
    /// Defines the request body
    var body: [String: String]? { get }
    
    /// Relative to the method we want to call, that was defined with an enum above
    var method: HTTPMethod { get }
    
}
