import UIKit
import Extensions
import Components
import Core
import DesignSystem

public class LoginViewController: UIViewController {
    
    private enum InputLabels {
        static let email = "Email"
        static let password = "Senha"
        static let emailFormatError = "Endereço de email inválido"
    }
    
    private enum Placeholders {
        static let email = "Email"
        static let password = "Senha"
    }
    
    //MARK: - Private variables
    
    private var viewModel: LoginViewModel
    
    private lazy var loading: LoadingIndicator = {
        let loading = LoadingIndicator(frame: CGRect())
        return loading
    }()
    
    private lazy var bgImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "loginBg")
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var whiteBg: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.white
        imageView.isHidden = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let normalText = NSMutableAttributedString(string:"Você está no Empresas.", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: Metrics.Size.mediumFont)])
        let boldText = NSMutableAttributedString(string:"Boas vindas,\n", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: Metrics.Size.largeTitleFont)])
        boldText.append(normalText)
        
        let label = UILabel()
        label.attributedText = boldText
        label.numberOfLines = 2
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var instructionLabel: UILabel = {
        let label = UILabel()
        label.text = "Digite seus dados para continuar."
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var emailLabel: InputLabel = {
        let label = InputLabel()
        label.text = InputLabels.email
        return label
    }()
    
    private lazy var emailTextField: InputTextField = {
        let textField = InputTextField()
        textField.delegate = self
        textField.placeholder = Placeholders.email
        textField.addTarget(self, action: #selector(emailDidChange), for: .editingChanged)
        return textField
    }()
    
    private lazy var emailErrorLabel: ErrorLabel = {
        let label = ErrorLabel()
        label.text = InputLabels.emailFormatError
        return label
    }()
    
    private lazy var passwordLabel: InputLabel = {
        let label = InputLabel()
        label.text = InputLabels.password
        return label
    }()
    
    private lazy var passwordTextField: InputTextField = {
        let textField = InputTextField()
        textField.delegate = self
        textField.placeholder = Placeholders.password
        textField.enablePasswordToggle()
        textField.addTarget(self, action: #selector(passwordDidChange), for: .editingChanged)
        textField.isSecureTextEntry = true
        return textField
    }()
    
    private lazy var loginButton: UIButton = {
        let button = UIButton()
        button.setTitle("ENTRAR", for: .normal)
        button.layer.cornerRadius = Metrics.Style.mediumRadius
        button.backgroundColor = UIColor.disabledButtonBg
        button.addTarget(self, action: #selector(confirmLogin), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var loginStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Metrics.Margin.medium
        stack.backgroundColor = UIColor.white
        stack.isLayoutMarginsRelativeArrangement = true
        stack.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: Metrics.Margin.default,
            leading: Metrics.Margin.default,
            bottom: 74,
            trailing: Metrics.Margin.default
        )
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var emailInputStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var passwordInputStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    //MARK: - Initializers
    
    public init(viewModel: LoginViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: - Lifecycle

    public override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavigationBar()
        
        configureBgImage()
        
        configureLoginStackView()
        
        configureInputStackViews()
        
        configureLoginButton()
        
        configureTitleLabel()
        
        configureLoadingIndicator()
        
        handleKeyboard()
        
        handleStateChange()
        viewModel.didUpdateViewState = { [weak self] in
            DispatchQueue.main.async {
                self?.handleStateChange()
            }
        }
    }
    
    //MARK: - Configuration methods
    
    private func configureNavigationBar() {
        navigationController?.navigationBar.barStyle = .black
    }
    
    private func configureBgImage() {
        view.addSubview(bgImage)
        
        NSLayoutConstraint.activate([
            bgImage.topAnchor.constraint(equalTo: view.topAnchor),
            bgImage.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            bgImage.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            bgImage.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ])
    }
    
    private func configureLoginStackView() {
        view.addSubview(loginStackView)
        
        NSLayoutConstraint.activate([
            loginStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            loginStackView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            loginStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ])
    }
    
    private func configureInputStackViews() {
        loginStackView.addArrangedSubviews(instructionLabel, emailInputStackView, passwordInputStackView)
        
        emailInputStackView.addArrangedSubviews(emailLabel, emailTextField, emailErrorLabel)
        
        passwordInputStackView.addArrangedSubviews(passwordLabel, passwordTextField)
        
    }
    
    private func configureLoginButton() {
        loginStackView.addArrangedSubview(loginButton)
        
        NSLayoutConstraint.activate([
            loginButton.heightAnchor.constraint(equalToConstant: Metrics.Size.defaultHeight),
        ])
    }
    
    private func configureTitleLabel() {
        view.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.bottomAnchor.constraint(equalTo: loginStackView.topAnchor, constant: -32),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24)
        ])
    }
    
    private func configureLoadingIndicator() {
        view.addSubview(whiteBg)
        view.addSubview(loading)
        
        NSLayoutConstraint.activate([
            
            //White background constraints
            whiteBg.topAnchor.constraint(equalTo: view.topAnchor),
            whiteBg.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            whiteBg.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            whiteBg.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            
            //Loading acticity indicator constraints
            loading.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loading.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        
    }
    
    //MARK: - Actions
    
    @objc
    private func emailDidChange() {
        if !emailErrorLabel.isHidden {
            emailErrorLabel.isHidden = true
            emailTextField.layer.borderColor = UIColor.secondary.cgColor
            emailTextField.textColor = UIColor.label
            emailLabel.textColor = #colorLiteral(red: 0.4034260213, green: 0.4159970284, blue: 0.4241061807, alpha: 1)
            
        }
        if emailLabel.isHidden {
            emailLabel.isHidden = false
        }
        
        changeLoginButtonBg()
    }
    
    @objc
    private func passwordDidChange() {
        if passwordLabel.isHidden {
            passwordLabel.isHidden = false
        }
        
        changeLoginButtonBg()
    }
    
    @objc
    private func confirmLogin() {
        view.endEditing(true)
        
        //Verify if any textfield is empty
        if (emailTextField.isEmpty() || passwordTextField.isEmpty()) {
            
            let alert = viewModel.createAlert(title: nil, message: "Por favor, preencha todos os campos", buttonText: "Fechar", style: .destructive)
            
            present(alert, animated: true)
            
            return
        }
        
        //Verify if email is valid
        if !emailTextField.text!.isValidEmail() {
            emailErrorLabel.isHidden = false
            emailTextField.layer.borderColor = UIColor.error.cgColor
            emailTextField.textColor = UIColor.error
            emailLabel.textColor = UIColor.error
            return
        }
        
        //Call api to authenticate and push to Companies
        viewModel.authenticate(with: User(email: emailTextField.text!, password: passwordTextField.text!))
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc
    private func dismissKeyb() {
        view.endEditing(true)
    }
    
    //MARK: - Private methods
    
    private func changeLoginButtonBg() {
        if (!emailTextField.isEmpty() && !passwordTextField.isEmpty()) {
            loginButton.backgroundColor = UIColor.enabledButtonBg
        }
        if (emailTextField.isEmpty() && passwordTextField.isEmpty() && loginButton.backgroundColor == UIColor.enabledButtonBg) {
            loginButton.backgroundColor = UIColor.disabledButtonBg
        }
    }
    
    private func handleStateChange() {
        switch viewModel.state {
        case .onboarding:
            break
        case .done:
            loading.stopAnimating()
        case .failure:
            hideLoadingIndicator()
            let alert = viewModel.createAlert(title: "Falha no login", message: "Por favor, verifique suas informações e a conexão com a internet", buttonText: "Tentar Novamente", style: .default)
            present(alert, animated: true)
        case .loading:
            showLoadingIndicator()
        }
    }
    
    private func handleKeyboard() {
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyb))
        view.addGestureRecognizer(tapGesture)
    }
    
    private func showLoadingIndicator() {
        DispatchQueue.main.async {
            self.whiteBg.isHidden = false
            self.loading.startAnimating()
            self.loading.isHidden = false
        }
    }
    
    private func hideLoadingIndicator() {
        DispatchQueue.main.async {
            self.whiteBg.isHidden = true
            self.loading.stopAnimating()
            self.loading.isHidden = true
        }
    }
}

//MARK: - Extensions

extension LoginViewController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        confirmLogin()
        return false
    }
}
