import Foundation
import UIKit
import Core

public extension LoginViewModel {
    enum ViewState: String, CaseIterable {
        case onboarding
        case done
        case failure
        case loading
    }
}

public final class LoginViewModel {
    
    // MARK: - Services
    
    private var authService: AuthServiceProtocol
    
    // MARK: - Private Variables
    
    private(set) var state: ViewState = .onboarding {
        didSet {
            didUpdateViewState?()
        }
    }
    
    //MARK: -Public Variables
    
    public var didUpdateViewState: (() -> Void)?
    
    public var pushToCompanies: (AuthResponse) -> Void
    
    //MARK: - Initializers
    
    public init(authService: AuthServiceProtocol, pushToCompanies: @escaping (AuthResponse) -> Void) {
        self.authService = authService
        self.pushToCompanies = pushToCompanies
    }
    
    //MARK: - API methods
    
    func authenticate(with user: User) {
        state = .loading
        
        authService.authPost(email: user.email, password: user.password) { [weak self] result in
            switch result {
            case .success(let response):
                self?.state = .done
                DispatchQueue.main.async {
                    self?.pushToCompanies(response)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self?.state = .failure
            }
        }
    }
    
    //MARK: - Interface methods
    
    ///Handle alert creation after passing its parameters
    func createAlert(title:String?, message: String?, buttonText: String, style: UIAlertAction.Style) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonText, style: style))
        return alert
    }
    
}
