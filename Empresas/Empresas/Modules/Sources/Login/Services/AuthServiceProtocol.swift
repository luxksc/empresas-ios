import Foundation
import Core

public protocol AuthServiceProtocol {
    func authPost(email: String, password: String, completion: @escaping (Result<AuthResponse, Error>) -> Void)
}
