import Foundation
import UIKit
import Core

public final class DetailsViewModel {
    
    // MARK: - Private variables
    
    private(set) var company: Company
    
    //MARK: - Initializers
    
    public init(company: Company) {
        self.company = company
    }
}
