import Foundation
import UIKit
import Kingfisher
import DesignSystem

public class DetailsViewController: UIViewController {
    
    //MARK: - Private variables
    
    private var viewModel: DetailsViewModel
    
    private lazy var topBgImage: UIImageView = {
        let image = UIImageView()
        image.layer.masksToBounds = true
        image.image = UIImage(named: "lightBg")
        image.contentMode = .scaleAspectFill
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    private lazy var companyTypeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: Metrics.Size.defaultFont)
        label.text = viewModel.company.companyType.companyTypeName
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var companyPhoto: UIImageView = {
        let image = UIImageView()
        image.kf.setImage(
            with: URL(string: "https://empresas.ioasys.com.br\(viewModel.company.photo)")
        )
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = Metrics.Style.defaultRadius
        image.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        image.clipsToBounds = true
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    private lazy var scrollTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.white
        tableView.separatorStyle = .none
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    //MARK: - Initializers
    
    public init(viewModel: DetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Lifecycle
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavigationBar()
        
        configureTopBgImage()
        
        configureCompanyTypeLabel()
        
        configureCompanyPhoto()
        
        configureScrollTableView()
        
        registerCells()
        
    }
    
    //MARK: - Configuration methods
    
    private func configureNavigationBar() {
        
        title = viewModel.company.companyName
        
        navigationController?.overrideUserInterfaceStyle = .dark
        
        if let navBar = navigationController?.navigationBar {
            navBar.isHidden = false
            
            let titleTextAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: Metrics.Size.mediumFont), NSAttributedString.Key.foregroundColor: UIColor.white]
            
            navBar.standardAppearance.titleTextAttributes = titleTextAttributes
            navBar.scrollEdgeAppearance?.titleTextAttributes = titleTextAttributes
            
            navBar.tintColor = UIColor.white
        }
        
        configureBackwardButton()
    }
    
    private func configureBackwardButton() {
        let backwardButton = UIBarButtonItem(image: UIImage.init(systemName: "arrow.backward"), style: .plain, target: self, action: #selector(dismissView))
        
        backwardButton.tintColor = UIColor.white
        
        navigationItem.leftBarButtonItem = backwardButton
    }
    
    private func configureTopBgImage() {
        view.addSubview(topBgImage)
        
        NSLayoutConstraint.activate([
            topBgImage.topAnchor.constraint(equalTo: view.topAnchor),
            topBgImage.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            topBgImage.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Metrics.Size.defaultHeight),
            topBgImage.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ])
    }
    
    private func configureCompanyTypeLabel() {
        view.addSubview(companyTypeLabel)
        
        NSLayoutConstraint.activate([
            companyTypeLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            companyTypeLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
            
        ])
    }
    
    private func configureCompanyPhoto() {
        view.addSubview(companyPhoto)
        
        NSLayoutConstraint.activate([
            companyPhoto.topAnchor.constraint(equalTo: topBgImage.bottomAnchor),
            companyPhoto.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            companyPhoto.heightAnchor.constraint(equalToConstant: 244),
            companyPhoto.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ])
    }
    
    private func configureScrollTableView() {
        view.addSubview(scrollTableView)
        
        NSLayoutConstraint.activate([
            scrollTableView.topAnchor.constraint(equalTo: companyPhoto.bottomAnchor, constant: Metrics.Margin.medium),
            scrollTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Metrics.Margin.default),
            scrollTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Metrics.Margin.default),
            scrollTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    //MARK: - Actions
    
    @objc
    private func dismissView() {
        dismiss(animated: true)
    }
    
    //MARK: - Private methods
    
    private func registerCells() {
        
        scrollTableView.register(ScrollableLabelTableViewCell.self, forCellReuseIdentifier: ScrollableLabelTableViewCell.identifier)
        
    }
    
}

//MARK: - Extensions

extension DetailsViewController: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = scrollTableView.dequeueReusableCell(withIdentifier: ScrollableLabelTableViewCell.identifier, for: indexPath) as? ScrollableLabelTableViewCell else { return UITableViewCell()}
        
        cell.backgroundColor = UIColor.white
        
        cell.setCellData(with: viewModel.company.description)
        
        cell.heightAnchor.constraint(equalTo: cell.descriptionLabel.heightAnchor).isActive = true
        
        return cell
    }
}
