import Foundation
import UIKit
import DesignSystem

public class ScrollableLabelTableViewCell: UITableViewCell {
    
    static let identifier: String = "ScrollableLabelTableViewCell"
    
    //MARK: - Private variables
    
    public lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: Metrics.Size.defaultFont)
        label.textAlignment = .justified
        label.textColor = UIColor.neutral
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    //MARK: - Initializers
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        configureLabel()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Configuration methods
    
    private func configureLabel() {
        addSubview(descriptionLabel)
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: topAnchor),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor)
        ])
    }
    
    //MARK: - Private methods
    
    func setCellData(with text: String) {
        descriptionLabel.text = text
    }
}
