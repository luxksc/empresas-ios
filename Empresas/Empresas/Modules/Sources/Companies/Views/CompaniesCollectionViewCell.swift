import Foundation
import UIKit
import Kingfisher
import Core
import DesignSystem

public class CompaniesCollectionViewCell: UICollectionViewCell {
    
    static let identifier: String  = "CompaniesCollectionViewCell"
    
    // MARK: - Private variables
    
    private lazy var mainView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.layer.cornerRadius = Metrics.Style.defaultRadius
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var bgImage: UIImageView = {
        let image = UIImageView()
        image.layer.masksToBounds = true
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: "lightBg")
        image.layer.cornerRadius = Metrics.Style.defaultRadius
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    public lazy var companyImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.layer.cornerRadius = Metrics.Style.defaultRadius
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    public lazy var companyNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: Metrics.Size.defaultFont)
        label.textColor = UIColor.neutral
        label.textAlignment = .center
        label.clipsToBounds = true
        label.backgroundColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    //MARK: - Initializers
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        configureMainView()
        configureBgImage()
        configureCompanyImage()
        configureCompanyNameLabel()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Configuration methods
    
    private func configureMainView() {
        addSubview(mainView)
        
        NSLayoutConstraint.activate([
            mainView.topAnchor.constraint(equalTo: topAnchor),
            mainView.trailingAnchor.constraint(equalTo: trailingAnchor),
            mainView.bottomAnchor.constraint(equalTo: bottomAnchor),
            mainView.leadingAnchor.constraint(equalTo: leadingAnchor),
        ])
    }
    
    private func configureBgImage() {
        mainView.addSubview(bgImage)
        
        NSLayoutConstraint.activate([
            bgImage.heightAnchor.constraint(equalToConstant:107),
            bgImage.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
            bgImage.bottomAnchor.constraint(equalTo: mainView.bottomAnchor),
            bgImage.leadingAnchor.constraint(equalTo: mainView.leadingAnchor)
        ])
    }
    
    private func configureCompanyImage() {
        mainView.addSubview(companyImage)
        
        NSLayoutConstraint.activate([
            companyImage.topAnchor.constraint(equalTo: mainView.topAnchor),
            companyImage.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -Metrics.Margin.medium),
            companyImage.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: -Metrics.Margin.small),
            companyImage.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: Metrics.Margin.medium),
        ])
    }
    
    private func configureCompanyNameLabel() {
        mainView.addSubview(companyNameLabel)
        
        NSLayoutConstraint.activate([
            companyNameLabel.heightAnchor.constraint(equalToConstant: 33),
            companyNameLabel.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
            companyNameLabel.bottomAnchor.constraint(equalTo: mainView.bottomAnchor),
            companyNameLabel.leadingAnchor.constraint(equalTo: mainView.leadingAnchor)
        ])
    }
    
    //MARK: - Private methods
    
    func setCellData(with company: Company) {
        
        let imageURL = URL(string: "https://empresas.ioasys.com.br\(company.photo)")
        
        companyImage.kf.setImage(with: imageURL)
        
        companyNameLabel.text = company.companyName
    }
}
