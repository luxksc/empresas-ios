import Foundation
import UIKit
import DesignSystem

public class EmptyMessageView: UIView {
    
    //MARK: - Private variables
    
    private lazy var emptyImageView: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "emptyImage")
        image.contentMode = .scaleAspectFill
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    private lazy var emptyLabel: UILabel = {
        let label = UILabel()
        label.text = "Empresa não encontrada"
        label.textColor = UIColor.neutral2
        label.font = UIFont.systemFont(ofSize: Metrics.Size.defaultFont)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Metrics.Margin.small
        stack.axis = .vertical
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    //MARK: - Initializers
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        configureStackView()
        
        configureEmptyImage()
        
        configureEmptyLabel()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Configuration methods
    
    private func configureStackView() {
        addSubview(contentStackView)
        
        NSLayoutConstraint.activate([
            contentStackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            contentStackView.topAnchor.constraint(equalTo: topAnchor, constant: 80)
        ])
    }
    
    private func configureEmptyImage() {
        contentStackView.addArrangedSubview(emptyImageView)
        
        NSLayoutConstraint.activate([
            emptyImageView.widthAnchor.constraint(equalToConstant: 212),
            emptyImageView.heightAnchor.constraint(equalToConstant: 150)
        ])
    }
    
    private func configureEmptyLabel() {
        contentStackView.addArrangedSubview(emptyLabel)
    }
}
