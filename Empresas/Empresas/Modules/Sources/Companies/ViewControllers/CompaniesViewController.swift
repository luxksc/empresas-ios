import UIKit
import Components
import Extensions
import DesignSystem

public class CompaniesViewController: UIViewController {
    
    //MARK: - Private variables
    
    private var viewModel: CompaniesViewModel
    
    private var lastSearch: String = ""
    
    private lazy var loading: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.frame = CGRect(x: 0, y: 8, width: Metrics.Size.defaultFont, height: Metrics.Size.defaultFont)
        indicator.color = .black
        return indicator
    }()
    
    private lazy var emptyMessageView: EmptyMessageView = {
        let view = EmptyMessageView()
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.text = viewModel.largeTitle
        label.textColor = UIColor.primary
        label.font = UIFont.boldSystemFont(ofSize: Metrics.Size.largeTitleFont)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var searchTextField: InputTextField = {
        let textField = InputTextField()
        textField.placeholder = "Buscar..."
        textField.delegate = self
        textField.textColor = UIColor.neutral2
        textField.tintColor = #colorLiteral(red: 0.5667684078, green: 0.5668845177, blue: 0.5704726577, alpha: 1)
        textField.setLeftIcon(with: UIImage.init(systemName: "magnifyingglass")!)
        return textField
    }()
    
    private lazy var collection: UICollectionView = {
        let viewWidth = view.frame.size.width
        let cardWidth = (viewWidth - Metrics.Margin.medium - 2 * Metrics.Margin.default) / 2
        
        let layout = viewModel.createCollectionLayout(cardWidth: cardWidth)
        
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = UIColor.white
        collection.translatesAutoresizingMaskIntoConstraints = false
        
        collection.register(CompaniesCollectionViewCell.self, forCellWithReuseIdentifier: CompaniesCollectionViewCell.identifier)
        
        return collection
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Metrics.Margin.medium
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var errorMessage: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: Metrics.Size.mediumFont)
        label.textColor = UIColor.neutral
        label.numberOfLines = 0
        label.text = """
                    Erro
                    Verifique a conexão
                    com sua internet
                    """
        label.textAlignment = .center
        return label
    }()
    
    //MARK: - Initializers
    
    public init(viewModel: CompaniesViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Lifecycle
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavigationBar()

        configureStackView()

        configureSearchTextField()
        
        configureCollection()
        
        handleStateChange()
        viewModel.didUpdateViewState = { [weak self] in
            DispatchQueue.main.async {
                self?.handleStateChange()
            }
        }
    
    }
    
    //MARK: - Configuration methods
    
    private func configureNavigationBar() {
        
        navigationItem.setHidesBackButton(true, animated: false)
        
        title = viewModel.title
        
        if let navBar = navigationController?.navigationBar {
            viewModel.configureNavigationBar(navBar)
        }
        
        let backwardButton = UIBarButtonItem(image: UIImage.init(systemName: "arrow.backward"), style: .plain, target: self, action: #selector(handleBackwardButton))
        
        backwardButton.tintColor = UIColor.secondary
        
        navigationItem.leftBarButtonItem = backwardButton
        
        navigationController?.navigationBar.isHidden = true
    }
    
    private func configureStackView() {
        view.addSubview(contentStackView)
        
        NSLayoutConstraint.activate([
            contentStackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Metrics.Margin.medium),
            contentStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Metrics.Margin.default),
            contentStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Metrics.Margin.default)
        ])
        
        contentStackView.addArrangedSubviews(titleLabel, searchTextField)
    }
    
    private func configureSearchTextField() {
        
        NSLayoutConstraint.activate([
            searchTextField.heightAnchor.constraint(equalToConstant: Metrics.Size.defaultHeight)
        ])
    }
    
    private func configureCollection() {
        view.addSubview(collection)
        
        NSLayoutConstraint.activate([
            collection.topAnchor.constraint(equalTo: contentStackView.bottomAnchor),
            collection.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collection.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            collection.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    
    // MARK: - Actions
    
    @objc
    private func dismissKeyb() {
        view.endEditing(true)
    }
    
    @objc
    private func handleBackwardButton() {
        navigationController?.navigationBar.isHidden = true
        
        titleLabel.isHidden = false
        
        searchTextField.setLeftIcon(with: UIImage.init(systemName: "magnifyingglass")!)
        
        searchTextField.text = ""
        lastSearch = ""
        
        view.endEditing(true)
        
        viewModel.backToOnboarding()
    }
    
    //MARK: - Private methods
    
    private func handleStateChange() {
        collection.reloadData()
        
        switch viewModel.state {
        case .onboarding:
            collection.backgroundView = nil
            searchTextField.hideActivityIndicator(loading)
        case .done:
            collection.backgroundView = nil
            searchTextField.hideActivityIndicator(loading)
        case .empty:
            collection.backgroundView = emptyMessageView
            searchTextField.hideActivityIndicator(loading)
        case .failure:
            collection.backgroundView = errorMessage
            searchTextField.hideActivityIndicator(loading)
        case .loading:
            searchTextField.showActivityIndicator(loading)
        }
    }
    
    private func handleSearch() {
        if (!searchTextField.isEmpty()  && searchTextField.text != lastSearch) {
            let nameToSearch = searchTextField.text!
            
            lastSearch = nameToSearch
            
            viewModel.searchCompanies(with: nameToSearch)
        }
    }
    
}

// MARK: - Extensions

extension CompaniesViewController: UITextFieldDelegate {
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        titleLabel.isHidden = true
        
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: textField.frame.height))
        
        navigationController?.navigationBar.isHidden = false
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        handleSearch()
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }
}

extension CompaniesViewController: UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.companies.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collection.dequeueReusableCell(withReuseIdentifier: CompaniesCollectionViewCell.identifier, for: indexPath) as? CompaniesCollectionViewCell else {return UICollectionViewCell()}
        
        viewModel.setShadow(for: cell)
        
        let company = viewModel.companies[indexPath.row]
        
        cell.setCellData(with: company)
        
        return cell
    }
}

extension CompaniesViewController: UICollectionViewDelegate {
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let company = viewModel.companies[indexPath.row]
        viewModel.pushToDetails(company)
    }
}
