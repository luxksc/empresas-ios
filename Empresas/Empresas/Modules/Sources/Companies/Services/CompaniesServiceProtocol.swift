import Foundation
import Core

public protocol CompaniesServiceProtocol {
    func getCompanies(authToken: String, client: String, uid:String, companyName:String, completion: @escaping (Result<Companies, Error>) -> Void)
}
