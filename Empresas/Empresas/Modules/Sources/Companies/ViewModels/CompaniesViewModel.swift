import Foundation
import Core
import UIKit
import DesignSystem

public extension CompaniesViewModel {
    enum ViewState: String, CaseIterable {
        case onboarding
        case done
        case empty
        case failure
        case loading
    }
}

public final class CompaniesViewModel {
    
    //MARK: - Services
    
    private var getCompaniesService: CompaniesServiceProtocol
    
    //MARK: - Private variables
    
    private(set) var authResponse: AuthResponse
    
    private(set) var state: ViewState = .onboarding {
        didSet {
            didUpdateViewState?()
        }
    }
    
    //MARK: - Public variables
    
    public var companies: [Company] = []
    
    public let largeTitle = "Pesquise por\numa empresa"
    public let title = "Pesquise"
    
    public var didUpdateViewState: (() -> Void)?
    
    public var pushToDetails: (Company) -> Void
    
    //MARK: - Initializers
    
    public init(authResponse: AuthResponse, getCompaniesService: CompaniesServiceProtocol, pushToDetails: @escaping (Company) -> Void) {
        self.authResponse = authResponse
        self.getCompaniesService = getCompaniesService
        self.pushToDetails = pushToDetails
    }
    
    //MARK: - API methods
    
    func searchCompanies(with companyName: String) {
        state = .loading
        
        getCompaniesService.getCompanies(authToken: authResponse.accessToken, client: authResponse.client, uid: authResponse.uid, companyName: companyName) { [weak self] result in
            switch result {
            case .success(let response):
                if response.enterprises.count == 0 {
                    self?.companies = response.enterprises
                    self?.state = .empty
                } else {
                    self?.companies = response.enterprises
                    self?.state = .done
                }
            case .failure(let error):
                print(error.localizedDescription)
                self?.state = .failure
            }
        }
    }
    
    //MARK: - Interface methods
    
    func backToOnboarding() {
        companies = []
        state = .onboarding
    }
    
    func createCollectionLayout(cardWidth: CGFloat) -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: cardWidth, height: 144)
        layout.minimumInteritemSpacing = Metrics.Margin.medium
        layout.minimumLineSpacing = Metrics.Margin.medium
        layout.sectionInset = UIEdgeInsets(top: Metrics.Margin.medium, left: Metrics.Margin.default, bottom: 0, right: Metrics.Margin.default)
        return layout
    }
    
    func configureNavigationBar(_ navBar: UINavigationBar) {
        
        navBar.isHidden = false
        
        let titleTextAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: Metrics.Margin.default)]
        
        navBar.standardAppearance.titleTextAttributes = titleTextAttributes
        navBar.scrollEdgeAppearance?.titleTextAttributes = titleTextAttributes
    }
    
    func setShadow(for view: UIView) {
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = .zero
        view.layer.shadowRadius = 16
    }
}
