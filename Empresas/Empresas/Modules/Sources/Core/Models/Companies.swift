import Foundation

public struct Companies: Decodable {
    public let enterprises: [Company]
}

public struct Company: Decodable {
    public let id: Int
    public let companyName, photo, description: String
    public let companyType: CompanyType
    
    public init(
        id: Int,
        companyName: String,
        photo: String,
        description: String,
        companyType: CompanyType
    ) {
        self.id = id
        self.companyName = companyName
        self.photo = photo
        self.description = description
        self.companyType = companyType
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case companyName = "enterprise_name"
        case photo
        case description
        case companyType = "enterprise_type"
    }
}

public struct CompanyType: Decodable {
    public let id: Int
    public let companyTypeName: String
    
    public init(id: Int, companyTypeName: String) {
        self.id = id
        self.companyTypeName = companyTypeName
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case companyTypeName = "enterprise_type_name"
    }
}
