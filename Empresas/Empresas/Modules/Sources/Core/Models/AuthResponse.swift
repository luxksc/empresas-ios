import Foundation

public struct AuthResponse: Decodable {
    public let accessToken: String
    public let client: String
    public let uid: String
    
    public init(accessToken: String, client: String, uid: String) {
        self.accessToken = accessToken
        self.client = client
        self.uid = uid
    }
}

