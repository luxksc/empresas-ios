import Foundation
import UIKit

extension UITextField {
    
    ///Return a boolean after verify if the textfield is empty
    public func isEmpty() -> Bool {
        self.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ? true : false
    }
    
    //ACTIVITY INDICATOR
    
    public func showActivityIndicator(_ indicator: UIActivityIndicatorView) {
        
        indicator.startAnimating()
        indicator.isHidden = false
        
        let indicatorContainerView = UIView(frame: CGRect(x: 20, y: 0, width: 30, height: 30))
        indicatorContainerView.addSubview(indicator)
        
        rightView = indicatorContainerView
        rightViewMode = .always
    }
    
    public func hideActivityIndicator(_ indicator: UIActivityIndicatorView) {
        indicator.stopAnimating()
        indicator.isHidden = true
        rightViewMode = .never
    }
    
    ///Add an image to an UITextField left view
    public func setLeftIcon(with image: UIImage) {
        
        let iconView = UIImageView(frame: CGRect(x: 10, y: 8, width: 16, height: 16))
        iconView.image = image
        
        let iconContainerView = UIView(frame: CGRect(x: 20, y: 0, width: 30, height: 30))
        iconContainerView.addSubview(iconView)
        
        leftView = iconContainerView
    }
    
    // PASSWORD ICON
    fileprivate func setPasswordToggleImage(_ button: UIButton) {
        if isSecureTextEntry {
            button.setImage(UIImage(systemName: "eye"), for: .normal)
        } else {
            button.setImage(UIImage(systemName: "eye.slash"), for: .normal)
        }
    }
    
    public func enablePasswordToggle(){
        let button = UIButton(type: .custom)
        button.setImage(UIImage(systemName: "eye"), for: .normal)
        button.tintColor = #colorLiteral(red: 0.462745098, green: 0.462745098, blue: 0.4666666667, alpha: 1)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(self.frame.size.width - 16), y: CGFloat(5), width: CGFloat(16), height: CGFloat(16))
        button.addTarget(self, action: #selector(self.togglePasswordView), for: .touchUpInside)
        self.rightView = button
        self.rightViewMode = .always
    }
    
    @objc
    func togglePasswordView(_ sender: Any) {
        self.isSecureTextEntry = !self.isSecureTextEntry
        setPasswordToggleImage(sender as! UIButton)
    }
    
    
}
