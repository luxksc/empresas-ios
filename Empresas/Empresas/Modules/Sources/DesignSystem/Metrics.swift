import UIKit

public struct Metrics {
    
    public struct Margin {
        static public let small: CGFloat = 16
        
        static public let `default`: CGFloat = 24
        
        static public let medium: CGFloat = 32
    }
    
    public struct Size {
        static public let defaultHeight: CGFloat = 48
        
        static public let buttonFont: CGFloat = 14
        
        static public let smallFont: CGFloat = 12
        
        static public let defaultFont: CGFloat = 16
        
        static public let mediumFont: CGFloat = 24
        
        static public let titleFont: CGFloat = 28
        
        static public let largeTitleFont: CGFloat = 40
    }
    
    public struct Style {
        
        static public let smallRadius: CGFloat = 8
        
        static public let defaultRadius: CGFloat = 16
        
        static public let mediumRadius: CGFloat = 24
        
        static public let borderWidth: CGFloat = 1
    }
    
}
