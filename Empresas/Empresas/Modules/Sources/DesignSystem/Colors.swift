import UIKit

extension UIColor {
    
    ///Project primary color
    static public let primary: UIColor = #colorLiteral(red: 0.2039215686, green: 0.08235294118, blue: 0.1294117647, alpha: 1) //#341521
    
    ///Project secondary color
    static public let secondary: UIColor = #colorLiteral(red: 0.6705882353, green: 0.3960784314, blue: 0.7882352941, alpha: 1) //#AB65C9
    
    ///Project primary neutral color
    static public let neutral: UIColor = #colorLiteral(red: 0.6431372549, green: 0.6431372549, blue: 0.6431372549, alpha: 1) //#A4A4A4
    
    ///Project secondary neutral color
    static public let neutral2: UIColor = #colorLiteral(red: 0.1333333333, green: 0.1411764706, blue: 0.1450980392, alpha: 1) //#222425
    
    ///Project color for error notifications
    static public let error: UIColor = #colorLiteral(red: 0.7960784314, green: 0.3921568627, blue: 0.3764705882, alpha: 1) //#CB6460
    
    ///Project color for button when it is activated
    static public let enabledButtonBg: UIColor = #colorLiteral(red: 0.1529411765, green: 0.06274509804, blue: 0.09803921569, alpha: 1) //#271019
    
    ///Project color for button when it is deactivated
    static public let disabledButtonBg: UIColor = #colorLiteral(red: 0.5411764706, green: 0.5411764706, blue: 0.5411764706, alpha: 1) //#8A8A8A
    
    ///Project color for textfield border when it is not on focus
    static public let textFieldNeutralBorder: CGColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1) //#C4C4C4
    
}
