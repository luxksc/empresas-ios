// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Modules",
    platforms: [.iOS(.v13)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "Modules",
            targets: [
                "Core",
                "Networking",
                "Login",
                "Companies",
                "Details"
            ]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/onevcat/Kingfisher.git", from: .init(7, 0, 0))
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "Extensions",
            dependencies: []),
        .target(
            name: "Components",
            dependencies: ["DesignSystem"]),
        .target(
            name: "Core",
            dependencies: []),
        .target(
            name: "Networking",
            dependencies: ["Core"]),
        .target(
            name: "DesignSystem",
            dependencies: []),
        .target(
            name: "Login",
            dependencies: ["Extensions", "Components", "Core", "DesignSystem"]),
        .testTarget(
            name: "LoginTests",
            dependencies: ["Login"]),
        .target(
            name: "Companies",
            dependencies: ["Components", "Extensions", "Kingfisher", "Core", "DesignSystem"]),
        .testTarget(
            name: "CompaniesTests",
            dependencies: ["Companies"]),
        .target(
            name: "Details",
            dependencies: ["Core", "Kingfisher", "DesignSystem"]),
        .testTarget(
            name: "DetailsTests",
            dependencies: ["Details"]),
    ]
)
