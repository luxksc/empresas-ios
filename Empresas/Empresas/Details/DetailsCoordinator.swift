import Foundation
import UIKit
import Details
import Core

final class DetailsCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    
    private let company: Company
    
    private let navigationController: UINavigationController
    
    init(navigationController: UINavigationController, company: Company) {
        self.navigationController = navigationController
        self.company = company
    }
    
    func start() {
        let detailsViewModel = DetailsViewModel(company: company)
        
        let detailsViewController = UINavigationController(
            rootViewController: DetailsViewController(
                viewModel: detailsViewModel
            ))
        detailsViewController.view.backgroundColor = UIColor.white
        detailsViewController.modalPresentationStyle = .fullScreen
        
        navigationController.present(detailsViewController, animated: true)
    }
    
    
}
