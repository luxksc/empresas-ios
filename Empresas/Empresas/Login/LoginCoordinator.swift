import Foundation
import Login
import UIKit
import Core
import Companies

final class LoginCoordinator: Coordinator {
    
    private(set) var childCoordinators: [Coordinator] = []

    private let navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {

        let loginViewModel = LoginViewModel(authService: AuthService(), pushToCompanies: { authResponse in self.pushToCompanies(with: authResponse)})

        let loginViewController = LoginViewController(viewModel: loginViewModel)

        navigationController.setViewControllers([loginViewController], animated: true)
    }

    func pushToCompanies(with authResponse: AuthResponse) {

        let companiesCoordinator = CompaniesCoordinator(navigationController: navigationController, authResponse: authResponse)

        childCoordinators.append(companiesCoordinator)

        companiesCoordinator.start()

    }
}
