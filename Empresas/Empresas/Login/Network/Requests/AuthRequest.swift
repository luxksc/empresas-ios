import Foundation
import Networking

struct AuthRequest: URLRequestProtocol {
    
    private var email: String
    private var password: String
    
    init(email: String, password: String) {
        self.email = email
        self.password = password
    }
    
    var baseRequestURL: String {
        return "\(Constants.apiBaseURL)users/auth/sign_in"
    }
    
    var path: String? {
        return nil
    }
    
    var headers: [String: String]? {
        return ["Content-Type":"application/json"]
    }
    
    var body : [String: String]? {
        return ["email": self.email,
                "password": self.password]
    }
    
    var method: HTTPMethod {
        return .post
    }
    
    
}
