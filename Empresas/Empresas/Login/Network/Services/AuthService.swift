import Foundation
import Core
import Networking
import Login

struct AuthService: AuthServiceProtocol {
    
    func authPost(email: String, password: String, completion: @escaping (Result<AuthResponse, Error>) -> Void) {
        let request = AuthRequest(email: email, password: password)
        
        NetworkManager.shared.request(of: AuthResponse.self, request: request) { result in
            switch result {
            case .success(let result):
                completion(.success(result))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
}
